#!/usr/bin/env sh

set -e

if [ -z "$1" ] || [ "${1:0:1}" = '-' ]; then
	set -- widoco "$@"
fi

exec "$@"
