FROM alpine:latest

ARG WIDOCO_VERSION
# owl2json no longer maintained, so not versioning it

RUN apk add --no-cache openjdk8-jre && \
  wget https://github.com/dgarijo/Widoco/releases/download/v$WIDOCO_VERSION/widoco-$WIDOCO_VERSION-jar-with-dependencies.jar -O /opt/widoco.jar && \
  wget https://github.com/stain/owl2jsonld/releases/download/0.2.1/owl2jsonld-0.2.1-standalone.jar -O /opt/owl2jsonld.jar

COPY docker-entrypoint.sh jarize.sh /usr/local/bin/

RUN cd /usr/local/bin && \
  chmod +x docker-entrypoint.sh jarize.sh && \
  ln -s jarize.sh widoco && \
  ln -s jarize.sh owl2jsonld

WORKDIR /data

ENTRYPOINT ["docker-entrypoint.sh"]
