#!/usr/bin/env sh

set -e

JAR="/opt/$(basename $0).jar"

error() {
    echo >&2 "error: jar $JAR does not exist"
    exit 1
}

if [ ! -f "$JAR" ]; then
    error
fi

exec java -jar "$JAR" "$@"
